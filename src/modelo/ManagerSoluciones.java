package modelo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import modelo.comparador.ComparadorPorAlcance;
import modelo.comparador.ComparadorPorAleatoriedad;
import modelo.comparador.ComparadorPorPromedio;

public class ManagerSoluciones implements Observable {
	private List<Observador> observadores;
	public static Grafo grafo;
	private List<Set<Integer>> solucionesGolosas;
	private Set<Integer> solucionConFuerzaBruta;

	public ManagerSoluciones() {
		grafo = new Grafo();
		this.solucionesGolosas = new ArrayList<>();
		solucionConFuerzaBruta = new HashSet<>();
		observadores = new ArrayList<>();

	}

	public void agregarVertice(int vertice) {
		grafo.agregarVertice(vertice);
		notificarObservadores();
	}

	public void agregarArista(int origen, int destino) {
		grafo.agregarArista(origen, destino);
		notificarObservadores();
	}

	public void crearSolucionesGolosos() {
		SolucionGoloso sol1 = new SolucionGoloso(new ComparadorPorAleatoriedad(grafo));
		SolucionGoloso sol2 = new SolucionGoloso(new ComparadorPorAlcance(grafo));
		SolucionGoloso sol3 = new SolucionGoloso(new ComparadorPorPromedio(grafo));
		renovarSolucionesGolosas();
		solucionesGolosas.add(sol1.getConjuntoDominante());
		solucionesGolosas.add(sol2.getConjuntoDominante());
		solucionesGolosas.add(sol3.getConjuntoDominante());
		notificarObservadores();
	}
	private void renovarSolucionesGolosas() {
		solucionesGolosas = new ArrayList<>();
	}

	public void crearSolucionFuerzaBruta() {
		SolucionFuerzaBruta solucionFuerzaBruta = new SolucionFuerzaBruta();
		solucionConFuerzaBruta = solucionFuerzaBruta.getConjuntoDominanteMinimo();
		notificarObservadores();
	}

	public List<Integer> obtenerNodos() {
		return grafo.getNodos();
	}

	public List<Arista> obtenerArista() {
		return grafo.getAristas();
	}

	@Override
	public void notificarObservadores() {
		for (Observador o : observadores) {
			o.notificar();
		}
	}

	@Override
	public void agregarObservador(Observador o) {
		observadores.add(o);
		notificarObservadores();
	}

	public List<Set<Integer>> getSoluciones() {
		return solucionesGolosas;
	}

	public Set<Integer> getSolucionConFuerzaBruta() {
		return solucionConFuerzaBruta;
	}

}