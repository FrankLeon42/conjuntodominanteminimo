package modelo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SolucionFuerzaBruta {
	private Grafo grafo;
	private Set<Integer> subconjuntoActual;
	private List<Set<Integer>> posiblesConjuntos;
	private Set<Integer> _conjuntoDominanteMinimo;

	public SolucionFuerzaBruta() {
		grafo = ManagerSoluciones.grafo;
		subconjuntoActual = new HashSet<Integer>();
		posiblesConjuntos = new ArrayList<Set<Integer>>();
		generarElConjuntoDominanteMinimo();
	}

	private void generarElConjuntoDominanteMinimo() {
		generarCombinaciones(0);
		_conjuntoDominanteMinimo = encontrarElMinimo(posiblesConjuntos);
	}

	public void generarCombinaciones(int vertice) {
		if (vertice == grafo.tamano()) {
			if (esConjuntoDominante(subconjuntoActual))
				posiblesConjuntos.add(new HashSet<>(subconjuntoActual));
			return;
		} else {
			// Caso recursivo
			subconjuntoActual.add(vertice);
			generarCombinaciones(vertice + 1);

			subconjuntoActual.remove(vertice);
			generarCombinaciones(vertice + 1);
		}

	}

	private boolean esConjuntoDominante(Set<Integer> conjunto) {
		Set<Integer> nodosExplorados = new HashSet<Integer>();
		for (Integer elemento : conjunto) {
			nodosExplorados.addAll(alcanzables(elemento));
		}
		return nodosExplorados.size() == grafo.tamano();
	}

	private Set<Integer> alcanzables(int elem) {
		Set<Integer> alcanzados = new HashSet<>();
		for (Integer nodo : grafo.getNodos()) {
			if (grafo.hayArista(elem, nodo)) {
				alcanzados.add(nodo);
			}
		}
		return alcanzados;
	}

	public Set<Integer> encontrarElMinimo(List<Set<Integer>> listConjunto) {
		if (noHayMinimo()) {
			return new HashSet<Integer>();
		}
		Set<Integer> minimo = listConjunto.get(0);
		for (Set<Integer> conjunto : listConjunto) {
			if (conjunto.size() < minimo.size()) {
				minimo = conjunto;
			}
		}
		return minimo;
	}

	private boolean noHayMinimo() {
		return grafo.getAristas().size() == 0;
	}

	public Set<Integer> getConjuntoDominanteMinimo() {
		return _conjuntoDominanteMinimo;
	}

}
