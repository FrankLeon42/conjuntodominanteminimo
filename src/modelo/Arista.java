package modelo;

public class Arista {

	private int origen;
	private int destino;

	public Arista(int origen, int destino) {
		this.destino = destino;
		this.origen = origen;
	}

	public int getDestino() {
		return this.destino;
	}

	public int getOrigen() {
		return this.origen;
	}

}