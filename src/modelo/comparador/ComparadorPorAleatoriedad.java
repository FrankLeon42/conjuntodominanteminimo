package modelo.comparador;

import java.util.Comparator;
import java.util.Random;

import modelo.Grafo;

public class ComparadorPorAleatoriedad implements Comparator<Integer> {
	private Grafo grafo ;

	public ComparadorPorAleatoriedad(Grafo grafo) {
		this.grafo = grafo;
	}

	@Override
	public int compare(Integer o1, Integer o2) {
		Random rand = new Random();
		int random1 = rand.nextInt(grafo.tamano());
		int random2 = rand.nextInt(grafo.tamano());
		return Integer.compare(random1,random2);
	}

}
