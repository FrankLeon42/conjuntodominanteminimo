package modelo.comparador;

import java.util.Comparator;

import modelo.Grafo;

public class ComparadorPorAlcance implements Comparator<Integer> {
	private Grafo grafo;

	public ComparadorPorAlcance(Grafo grafo) {
		this.grafo = grafo;
	}

	@Override
	public int compare(Integer v1, Integer v2) {
		int alcanse1 = alcance(v1);
		int alcanse2 = alcance(v2);
		return Integer.compare(alcanse2, alcanse1);
	}
	public int alcance(int vertice) {
		return grafo.obtenerVecinos(vertice).size();
	}
}