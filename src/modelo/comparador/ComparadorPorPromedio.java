package modelo.comparador;

import java.util.Comparator;

import modelo.BFS;
import modelo.Grafo;

public class ComparadorPorPromedio implements Comparator<Integer> {
	private Grafo grafo;

	public ComparadorPorPromedio(Grafo grafo) {
		this.grafo = grafo;
	}

	@Override
	public int compare(Integer v1, Integer v2) {
		int promedio1 = calcularDistanciaPromedio(v1);
		int promedio2 = calcularDistanciaPromedio(v2);
		return Integer.compare(promedio1, promedio2);
	}

	public int calcularDistanciaPromedio(Integer vertice) {
		int totalDistancia = 0;
		for (Integer vecino : grafo.getNodos()) {
			totalDistancia += calcularDistancia(vertice, vecino);

		}
		return totalDistancia / grafo.tamano();
	}

	private int calcularDistancia(Integer v1, Integer v2) {
		return BFS.distanciaEntreVertices(grafo, v1, v2);
	}
}