package modelo;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class BFS {
	private static List<Integer> L;
	private static boolean[] marcados;

	public static boolean esConexo(Grafo g) {
		if (g == null)
			throw new IllegalArgumentException("El grafo no puede ser null.");

		return g.tamano() == 0 || alcanzables(g, 0).size() == g.tamano();
	}

	public static Set<Integer> alcanzables(Grafo g, int origen) {
		Set<Integer> ret = new HashSet<>();
		inicializarRecorrido(g, origen);

		while (!L.isEmpty()) {
			int i = L.get(0);
			marcados[i] = true;

			ret.add(i);
			agregarVecinosPendientes(g, i);
			L.remove(0);
		}
		return ret;
	}

	private static void agregarVecinosPendientes(Grafo g, int vertice) {
		if (g.obtenerVecinos(vertice) == null) {
			return;
		}
		for (int vecino : g.obtenerVecinos(vertice))
			if (!marcados[vecino] && !L.contains(vecino))
				L.add(vecino);
	}

	private static void inicializarRecorrido(Grafo g, int origen) {
		L = new LinkedList<Integer>();
		marcados = new boolean[g.tamano()];
		L.add(origen);
	}

	public static int distanciaEntreVertices(Grafo g, int origen, int destino) {
		inicializarRecorrido(g, origen);
		int distancia = 0;

		while (!L.isEmpty()) {

			for (int i = 0; i < L.size(); i++) {
				int actual = L.remove(0);
				marcados[actual] = true;

				if (actual == destino) {
					return distancia;
				}

				agregarVecinosPendientes(g, actual);
			}

			distancia++;
		}

		return -1;
	}

}