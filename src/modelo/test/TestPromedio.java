package modelo.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.util.HashSet;
import java.util.Set;
import org.junit.Test;
import modelo.ManagerSoluciones;

public class TestPromedio {
	@Test
	public void pruebaSolverPromedio() {
		ManagerSoluciones ms = new ManagerSoluciones();
		ms.agregarVertice(0);
		ms.agregarVertice(1);
		ms.agregarVertice(2);
		ms.agregarVertice(3);
		ms.agregarVertice(4);
		ms.agregarArista(0, 1);
		ms.agregarArista(0, 2);
		ms.agregarArista(0, 3);
		ms.agregarArista(3, 4);

		Set<Integer> conjuntoEsperado = new HashSet<>();
		conjuntoEsperado.add(0);
		conjuntoEsperado.add(3);
		ms.crearSolucionesGolosos();
		assertTrue(conjuntoEsperado.equals(ms.getSoluciones().get(2)));
	}

	@Test
	public void pruebaSolverPromedioFalso() {
		ManagerSoluciones ms = new ManagerSoluciones();
		ms.agregarVertice(0);
		ms.agregarVertice(1);
		ms.agregarVertice(2);
		ms.agregarVertice(3);
		ms.agregarVertice(4);
		ms.agregarArista(0, 1);
		ms.agregarArista(0, 2);
		ms.agregarArista(0, 3);
		ms.agregarArista(3, 4);

		Set<Integer> conjuntoEsperado = new HashSet<>();
		conjuntoEsperado.add(0);
		conjuntoEsperado.add(4);
		ms.crearSolucionesGolosos();
		assertFalse(conjuntoEsperado.equals(ms.getSoluciones().get(2)));
	}
}
