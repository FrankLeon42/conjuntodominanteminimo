package modelo.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import modelo.Grafo;

public class TestGrafo {

	@Test
	public void hayAristaBuenCamino() throws Exception {
		Grafo g = new Grafo();

		g.agregarVertice(0);
		g.agregarVertice(1);

		g.agregarArista(0, 1);

		assertTrue(g.hayArista(0, 1));
	}

	@Test
	public void noHayAristaBuenCamino() throws Exception {
		Grafo g = new Grafo();

		g.agregarVertice(0);
		g.agregarVertice(1);

		g.agregarArista(0, 1);

		assertFalse(g.hayArista(0, 2));
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearAristaMultiplesVeces() throws Exception {
		Grafo g = new Grafo();

		g.agregarVertice(0);
		g.agregarVertice(1);
		g.agregarVertice(2);

		g.agregarArista(0, 1);
		g.agregarArista(0, 2);
		g.agregarArista(0, 1);
	}

	@Test
	public void vecinos() throws Exception {
		Grafo g = new Grafo();

		g.agregarVertice(0);
		g.agregarVertice(1);
		g.agregarVertice(2);
		g.agregarVertice(3);
		g.agregarVertice(4);

		g.agregarArista(0, 1);
		g.agregarArista(0, 2);
		g.agregarArista(0, 3);
		g.agregarArista(0, 4);

		assertTrue(g.obtenerVecinos(0).size() == 4);

	}
}
