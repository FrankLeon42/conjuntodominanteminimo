package modelo.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import modelo.ManagerSoluciones;

public class TestAlcance {
	@Test
	public void pruebaSolverAlcance() {

		ManagerSoluciones ms = new ManagerSoluciones();
		ms.agregarVertice(0);
		ms.agregarVertice(1);
		ms.agregarVertice(2);
		ms.agregarVertice(3);
		ms.agregarVertice(4);
		ms.agregarArista(0, 1);
		ms.agregarArista(0, 2);
		ms.agregarArista(0, 3);
		ms.agregarArista(0, 4);

	
		Set<Integer> conjuntoEsperado = new HashSet<>();
		conjuntoEsperado.add(0);
		ms.crearSolucionesGolosos();

		assertTrue( conjuntoEsperado.equals(ms.getSoluciones().get(1)));
		}
	
	@Test
	public void pruebaSolverAlcanceFalso() {
		
		ManagerSoluciones ms = new ManagerSoluciones();
		ms.agregarVertice(0);
		ms.agregarVertice(1);
		ms.agregarVertice(2);
		ms.agregarVertice(3);
		ms.agregarVertice(4);
		ms.agregarArista(0, 1);
		ms.agregarArista(0, 2);
		ms.agregarArista(0, 3);
		ms.agregarArista(0, 4);

		
		Set<Integer> conjuntoEsperado = new HashSet<>();
		conjuntoEsperado.add(3);
		ms.crearSolucionesGolosos();
		
		assertFalse( conjuntoEsperado.equals(ms.getSoluciones().get(1)));
		}

}
