package modelo.test;

import org.junit.Test;

import modelo.BFS;
import modelo.Grafo;


public class TestBFS {

	@Test
	public void encontrarDistanciaBuenCamino() {
		Grafo g = new Grafo();
		
		g.agregarVertice(0);
		g.agregarVertice(1);
		g.agregarVertice(2);
		g.agregarVertice(3);
		g.agregarVertice(4);
		g.agregarVertice(5);
		
		g.agregarArista(0, 1);
		g.agregarArista(1, 2);
		g.agregarArista(2, 3);
		g.agregarArista(3, 4);
		g.agregarArista(4, 5);
		
		int  resp = BFS.distanciaEntreVertices(g,0,5);
		
		System.out.println(resp);
		

}}
