package modelo.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import modelo.ManagerSoluciones;

public class TestFuerzaBruta {
	@Test
	public void obtenerElCDMinimoVerdadero() {
		ManagerSoluciones ms = new ManagerSoluciones();
		ms.agregarVertice(1);
		ms.agregarVertice(2);
		ms.agregarVertice(3);
		ms.agregarArista(1,2);
		ms.agregarArista(2,3);
		
		Set<Integer>respuestaEsperada=new HashSet<Integer>();
		respuestaEsperada.add(2);
		ms.crearSolucionFuerzaBruta();
		Set<Integer> solucionFuerzaBruta=ms.getSolucionConFuerzaBruta();
		assertTrue(solucionFuerzaBruta.equals(respuestaEsperada));
	}
	
	@Test
	public void obtenerElCDMinimoFalso() {
		ManagerSoluciones ms = new ManagerSoluciones();
		ms.agregarVertice(1);
		ms.agregarVertice(2);
		ms.agregarVertice(3);
		ms.agregarVertice(4);
		ms.agregarVertice(5);
		ms.agregarVertice(6);
		ms.agregarArista(6,4);
		ms.agregarArista(4,3);
		ms.agregarArista(4,5);
		ms.agregarArista(5,2);
		ms.agregarArista(5,1);
		Set<Integer>respuestaEsperada=new HashSet<Integer>();
		respuestaEsperada.add(6);
		respuestaEsperada.add(5);
		respuestaEsperada.add(4);
		ms.crearSolucionFuerzaBruta();
		Set<Integer> solucionFuerzaBruta=ms.getSolucionConFuerzaBruta();
		assertFalse(solucionFuerzaBruta.equals(respuestaEsperada));
	}

	@Test
	public void obtenerElCDMinimoPruebaDisconexo() {
		ManagerSoluciones ms = new ManagerSoluciones();
		ms.agregarVertice(1);
		ms.agregarVertice(2);
		ms.agregarVertice(3);
		ms.agregarVertice(4);
		ms.agregarVertice(5);
		ms.agregarArista(2,3);
		ms.agregarArista(2,4);
		ms.agregarArista(2,5);
		Set<Integer>respuestaEsperada=new HashSet<Integer>();
		respuestaEsperada.add(1);
		respuestaEsperada.add(2);
		ms.crearSolucionFuerzaBruta();
		Set<Integer> solucionFuerzaBruta=ms.getSolucionConFuerzaBruta();
		assertTrue(solucionFuerzaBruta.equals(respuestaEsperada));
	}
}
