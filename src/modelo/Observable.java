package modelo;

public interface Observable {
	public void agregarObservador(Observador o);
	public void notificarObservadores();
}
