package modelo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Grafo {
	private List<Integer> nodos;
	private List<Arista> aristas;
	private Map<Integer, List<Integer>> vecinos;

	public Grafo() {
		aristas = new ArrayList<>();
		nodos = new ArrayList<>();
		vecinos = new HashMap<>();
	}

	public void agregarVertice(int nodo) {
		if (!nodos.contains(nodo)) {
			nodos.add(nodo);
			vecinos.put(nodo, new ArrayList<>());
		}
	}

	public void agregarArista(int origen, int destino) {
		if (validarArista(origen, destino)) {
			aristas.add(new Arista(origen, destino));
			aristas.add(new Arista(destino, origen));
			vecinos.get(destino).add(origen);
			vecinos.get(origen).add(destino);
		} else {
			throw new IllegalArgumentException(" ya existe una arista o ya esta el nodo");
		}
	}

	public boolean validarArista(int origen, int destino) {
		return (!hayArista(origen, destino) && nodos.contains(origen) && nodos.contains(destino) && origen != destino);
	}

	public boolean hayArista(int origen, int destino) {
		boolean ret = false;
		for (Arista aristaConPeso : aristas) {
			if (origen == destino || (aristaConPeso.getOrigen() == origen && aristaConPeso.getDestino() == destino)
					|| (aristaConPeso.getOrigen() == destino && aristaConPeso.getDestino() == origen)) {
				ret = true;
			}
		}
		return ret;
	}

	public List<Integer> obtenerVecinos(int nodo) {
		return vecinos.get(nodo);
	}

	public List<Integer> getNodos() {
		return this.nodos;
	}

	public int tamano() {
		return nodos.size();
	}

	public List<Arista> getAristas() {
		return this.aristas;
	}

}