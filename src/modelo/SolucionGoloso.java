package modelo;

import java.util.Comparator;
import java.util.HashSet;

import java.util.Set;

public class SolucionGoloso {
	private Set<Integer> _conjuntoDominante;
	private Comparator<Integer> comparador;
	private Grafo grafo ;

	public SolucionGoloso(Comparator<Integer> comparador ) {
		this._conjuntoDominante = new HashSet<>();
		this.comparador = comparador;
		this.grafo = ManagerSoluciones.grafo;
		conjuntoDominanteGoloso();
	}

	public void conjuntoDominanteGoloso() {
		if(grafo.getAristas().size() == 0) {
			return ;
		}
		_conjuntoDominante = new HashSet<>();
		grafo.getNodos().sort(comparador);
		int i = 0;
		int j = 1;
		while (cantDeVecinosDelConjunto() < grafo.tamano()) {
			int nodo = 0;
			if(BFS.esConexo(grafo)) {
				 nodo = grafo.getNodos().get(i);
			}else {
				 nodo = grafo.getNodos().get(grafo.getNodos().size() -j);
			}
			
			_conjuntoDominante.add(nodo);
			i++;
			j++;
		}

	}

	private int cantDeVecinosDelConjunto() {
		Set<Integer> nodosExplorados = new HashSet<>();
		for (Integer nodoDeConjunto : _conjuntoDominante) {
			nodosExplorados.addAll(alcanzables(nodoDeConjunto));
		}
		return nodosExplorados.size();
	}

	private Set<Integer> alcanzables(int elem) {
		Set<Integer> alcanzados = new HashSet<>();
		for (Integer nodo : grafo.getNodos()) {
			if (grafo.hayArista(elem, nodo)) {
				alcanzados.add(nodo);
			}
		}
		return alcanzados;
	}

	public int tamañoDeConjunto() {
		return _conjuntoDominante.size();
	}

	public Set<Integer> getConjuntoDominante() {
		return _conjuntoDominante;
	}

	
}