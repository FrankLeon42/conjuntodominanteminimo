package vista;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.util.List;
import javax.swing.Timer;
import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Efectos {

	public static void reprodBgm(String ruta, int veces, List<Clip> enReproduccion) {
		try {
			AudioInputStream audioInputStream = AudioSystem
					.getAudioInputStream(Efectos.class.getResourceAsStream(ruta));
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			enReproduccion.add(clip);
			clip.start();
			if (veces != 1)
				clip.loop(Clip.LOOP_CONTINUOUSLY);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void detenerSonidos(List<Clip> sonidos) {
		for (Clip sonido : sonidos) {
			if (sonido.isRunning())
				sonido.stop();
		}
	}

	public static JPanel crearFondo(JFrame pantalla, String rutaImagen) {
		JPanel panel = new JPanel() {

			@Override
			protected void paintComponent(Graphics g) {

				super.paintComponent(g);
				try {

					InputStream inputStream = getClass().getResourceAsStream(rutaImagen);
					BufferedImage imagenDeFondo = ImageIO.read(inputStream);

					g.drawImage(imagenDeFondo, 0, 0, getWidth(), getHeight(), this);

				} catch (Exception e) {
					e.printStackTrace();

				}
			}

		};
		panel.setBounds(0, 0, 882, 706);

		pantalla.getContentPane().add(panel);
		panel.setLayout(null);

		return panel;

	}

	public static void estiloDeBotones(List<JButton> botonesMenu) {
		for (JButton btn : botonesMenu) {
			btn.setBackground(new Color(166, 166, 166));
			btn.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseEntered(MouseEvent e) {
					btn.setBackground(Color.black);
					btn.setForeground(Color.white);
				}

				@Override
				public void mouseExited(MouseEvent e) {
					btn.setBackground(new Color(166, 166, 166));
					btn.setForeground(Color.black);
				}
			});
		}
	}

}