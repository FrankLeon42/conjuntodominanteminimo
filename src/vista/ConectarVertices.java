package vista;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import controlador.Controlador;
import modelo.ManagerSoluciones;
import modelo.Observador;

import javax.swing.JComboBox;
import javax.sound.sampled.Clip;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.ActionEvent;

public class ConectarVertices implements Observador {

	private JFrame frame;
	private List<Integer> vertices;
	private List<Clip> enReproduccion;

	private Controlador controlador;
	private ManagerSoluciones managerSoluciones;

	public ConectarVertices(Menu menu, Controlador controlador, ManagerSoluciones managerSoluciones) {
		this.managerSoluciones = managerSoluciones;
		this.controlador = controlador;
		managerSoluciones.agregarObservador(this);
		initialize(menu);
	}

	private void initialize(Menu menu) {

		// Inicializaciones
		enReproduccion = new ArrayList<>();
		JButton btn_generarArista = new JButton("CREAR ARISTA");
		btn_generarArista.setEnabled(false);
		List<JButton> botonesDelMenu = new ArrayList<>();
		botonesDelMenu.add(btn_generarArista);

		// Ventana
		frame = new JFrame();
		frame.setBounds(100, 100, 882, 706);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		// Panel
		JPanel panel = Efectos.crearFondo(frame, "/imagenes/fondoAristas.jpg");

		// Vertice origen
		JLabel lbl_origen = new JLabel("ORIGEN");
		lbl_origen.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_origen.setBounds(70, 208, 192, 38);
		panel.add(lbl_origen);

		JComboBox<String> cb_destino = new JComboBox<>();
		JComboBox<String> cb_origen = new JComboBox<>();

		cb_origen.addItem("Origen");

		for (Integer vertice : vertices) {
			String opcion = Integer.toString(vertice);
			cb_origen.addItem(opcion);

		}
		cb_origen.setBounds(70, 280, 170, 29);
		panel.add(cb_origen);

		// Vertice destino
		JLabel lbl_destino = new JLabel("DESTINO");
		lbl_destino.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_destino.setBounds(550, 208, 192, 38);
		panel.add(lbl_destino);

		cb_destino.addItem("Destino");
		for (Integer vertice : vertices) {
			String opcion = Integer.toString(vertice);
			cb_destino.addItem(opcion);
		}
		cb_destino.setBounds(572, 280, 170, 29);
		panel.add(cb_destino);

		cb_origen.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				verificarSeleccion(cb_destino, cb_origen, btn_generarArista);
			}
		});

		cb_destino.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				verificarSeleccion(cb_destino, cb_origen, btn_generarArista);
			}
		});

		// Boton Generar arista
		btn_generarArista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int origen = Integer.parseInt(cb_origen.getSelectedItem().toString());
				int destino = Integer.parseInt(cb_destino.getSelectedItem().toString());
				if (!controlador.validadArista(origen, destino)) {
					Efectos.reprodBgm("/sonidos/sf_noCreada.wav", 1, enReproduccion);
					JOptionPane.showMessageDialog(frame, "No se creo arista: no bucles ni aristas repetidas",
							"No se creo la arista", JOptionPane.INFORMATION_MESSAGE);
				} else {
					controlador.agregarArista(origen, destino);
					Efectos.reprodBgm("/sonidos/sf_creada.wav", 1, enReproduccion);
					JOptionPane.showMessageDialog(frame, "¡Arista creada con exito!", "Se creo la arista",
							JOptionPane.INFORMATION_MESSAGE);
				}
				cb_origen.setSelectedItem("Origen");
				cb_destino.setSelectedItem("Destino");

			}
		});
		btn_generarArista.setBounds(328, 405, 170, 38);
		panel.add(btn_generarArista);

		// JButton irAMenu
		JButton btn_irAMenu = new JButton("IR A MENU");
		botonesDelMenu.add(btn_irAMenu);
		btn_irAMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Efectos.reprodBgm("/sonidos/sf_click.wav", 1, enReproduccion);
				ManagerDeVentanas managerAMenu = new ManagerDeVentanas(ConectarVertices.this, menu);
				frame.dispose();
				managerAMenu.irAMenuDesdeAristas();
			}
		});
		btn_irAMenu.setBounds(328, 504, 170, 38);
		panel.add(btn_irAMenu);

		// Estilo de botones
		Efectos.estiloDeBotones(botonesDelMenu);
	}

	public void setVisible(boolean visible) {
		frame.setVisible(visible);
	}

	@SuppressWarnings("rawtypes")
	private void verificarSeleccion(JComboBox cb_destino, JComboBox cb_origen, JButton btn_generarArista) {
		if (((String) cb_destino.getSelectedItem()).equals("Destino")
				|| ((String) cb_origen.getSelectedItem()).equals("Origen")) {
			btn_generarArista.setEnabled(false);
		} else {
			btn_generarArista.setEnabled(true);
		}
	}

	@Override
	public void notificar() {
		vertices = managerSoluciones.obtenerNodos();
	}

}
