package vista;

import modelo.ManagerSoluciones;

public class ManagerDeVentanas {
//	public static ManagerSoluciones soluciones;
	private Menu menu = null;
	private ConectarVertices aristas;
	private VerGrafo grafo;
	
	
	  //Conexion menu->aristas
	  public ManagerDeVentanas(Menu menu,ConectarVertices aristas) {
	    this.aristas=aristas;
	    this.menu=menu;
	    aristas.setVisible(false);
	  }

	  //Conexion aristas->menu
	  public ManagerDeVentanas(ConectarVertices aristas,Menu menu) {
	    this.aristas=aristas;
	    this.menu=menu;
	    menu.setVisible(false);
	  }
	  
	  //Conexion menu->grafo
	  public ManagerDeVentanas(Menu menu,VerGrafo grafo) {;
	    this.menu=menu;
	    this.grafo=grafo;
	    grafo.setVisible(false);
	  }

	  //Conexion grafo->menu
	  public ManagerDeVentanas(VerGrafo grafo,Menu menu) {
	    this.grafo=grafo;
		this.menu=menu;
	    menu.setVisible(false);
	  } 
	  
	  //Visibiliza grupos y oculta el menu 
	  public void irAAristas() {
	      menu.setVisible(false);
	      aristas.setVisible(true);
	  }
	  //Visibiliza menu y oculta Aristas
	  public void irAMenuDesdeAristas() {
		  aristas.setVisible(false);
		  menu.setVisible(true);
	  }
	  
	  //Visibiliza grafo y oculta el menu 
	  public void irAGrafo() {
	      menu.setVisible(false);
	      grafo.setVisible(true);
	  }
	  //Visibiliza menu y oculta grafo
	  public void irAMenuDesdeGrafo() {
		  grafo.setVisible(false);
		  menu.setVisible(true);
	  }
}
