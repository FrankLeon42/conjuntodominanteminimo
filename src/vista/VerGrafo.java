package vista;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.sound.sampled.Clip;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import modelo.Arista;
import modelo.ManagerSoluciones;
import modelo.Observador;
import modelo.SolucionGoloso;
import modelo.comparador.ComparadorPorAlcance;
import modelo.comparador.ComparadorPorAleatoriedad;
import modelo.comparador.ComparadorPorPromedio;

import javax.swing.JTextField;
import javax.swing.SwingConstants;

import controlador.Controlador;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;

public class VerGrafo implements Observador {

	private JFrame frame;
	private List<Arista> aristas;
	private List<Integer> nodos;
	private List<Set<Integer>> solucionesGolosas;
	private Set<Integer> solucionFuerzaBruta;
	private List<JButton> botonesDeNodos;
	private Map<Integer, Point> nodosConCoordenadas;
	private JPanel panel_grafo;
	private ManagerSoluciones managerSoluciones;
	private Controlador controlador;

	public VerGrafo(Menu menu, ManagerSoluciones managerSoluciones, Controlador controlador) {
		this.managerSoluciones = managerSoluciones;
		this.controlador = controlador;
		managerSoluciones.agregarObservador(this);
		initialize(menu);
	}

	private void initialize(Menu menu) {
		// Inicializaciones
		controlador.crearSoluciones();
		List<Clip> enReproduccion = new ArrayList<>();

		botonesDeNodos = new ArrayList<>();
		nodosConCoordenadas = new HashMap<>();
		List<JButton> botonesMenu = new ArrayList<>();

		// Ventana
		frame = new JFrame();
		frame.setBounds(100, 100, 882, 706);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		// JPanel Grafo
		panel_grafo = new JPanel() {
			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);

				for (Arista arista : aristas) {
					Point posicionOrigen = nodosConCoordenadas.get(arista.getOrigen());
					Point posicionDestino = nodosConCoordenadas.get(arista.getDestino());
					g.drawLine((int) posicionOrigen.getX() + 25, (int) posicionOrigen.getY() + 25,
							(int) posicionDestino.getX() + 25, (int) posicionDestino.getY() + 25);
				}
			}
		};
		panel_grafo.setBounds(20, 24, 578, 524);
		frame.getContentPane().add(panel_grafo);
		panel_grafo.setLayout(null);

		ubicarNodos();
		ponerBotonesEnElPanel(panel_grafo);
		dibujarLineas();

		// Jpanel menu
		JPanel panel_menu = new JPanel();
		panel_menu.setBounds(0, 585, 866, 71);
		frame.getContentPane().add(panel_menu);
		panel_menu.setLayout(null);

		// Boton irAMenu
		JButton btn_irAMenu = new JButton("IR A MENU");
		btn_irAMenu.setBounds(365, 11, 113, 49);
		panel_menu.add(btn_irAMenu);
		botonesMenu.add(btn_irAMenu);

		// Botones de tipo de solucion
		JButton btn_aleatoria = new JButton("ALEATORIA");
		btn_aleatoria.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Efectos.reprodBgm("/sonidos/sf_click.wav", 1, enReproduccion);
				limpiarNodos();
				mostrarConjunto(0);
			}
		});
		btn_aleatoria.setBounds(638, 159, 176, 35);
		frame.getContentPane().add(btn_aleatoria);
		botonesMenu.add(btn_aleatoria);

		JButton btn_promedio = new JButton("PROMEDIO");
		btn_promedio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Efectos.reprodBgm("/sonidos/sf_click.wav", 1, enReproduccion);
				limpiarNodos();
				mostrarConjunto(2);
			}
		});
		btn_promedio.setBounds(638, 253, 176, 35);
		frame.getContentPane().add(btn_promedio);
		botonesMenu.add(btn_promedio);

		JButton btn_alcance = new JButton("ALCANCE");
		btn_alcance.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Efectos.reprodBgm("/sonidos/sf_click.wav", 1, enReproduccion);
				limpiarNodos();
				mostrarConjunto(1);
			}
		});
		btn_alcance.setBounds(638, 349, 176, 35);
		frame.getContentPane().add(btn_alcance);
		botonesMenu.add(btn_alcance);

		JButton btn_FuerzaBruta = new JButton("FUERZA BRUTA");
		btn_FuerzaBruta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Efectos.reprodBgm("/sonidos/sf_click.wav", 1, enReproduccion);
				limpiarNodos();
				mostrarConjunto(3);
			}
		});
		btn_FuerzaBruta.setBounds(638, 450, 176, 35);
		frame.getContentPane().add(btn_FuerzaBruta);
		botonesMenu.add(btn_FuerzaBruta);

		btn_irAMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				Efectos.reprodBgm("/sonidos/sf_click.wav", 1, enReproduccion);
				ManagerDeVentanas managerAMenu = new ManagerDeVentanas(VerGrafo.this, menu);
				frame.dispose();
				managerAMenu.irAMenuDesdeGrafo();

			}
		});

		Efectos.estiloDeBotones(botonesMenu);
	}

	public void ubicarNodos() {
		for (Integer nodo : nodos) {
			agregarBoton("" + nodo);
		}
	}

	public void ponerBotonesEnElPanel(JPanel panel) {
		for (JButton boton : botonesDeNodos) {
			panel.add(boton);
		}
	}

	public void agregarBoton(String texto) {
		JButton botonARevisar = new JButton(texto);
		Random random = new Random();
		int x, y;
		do {
			x = random.nextInt(panel_grafo.getWidth() - 50);
			y = random.nextInt(panel_grafo.getHeight() - 50);
			botonARevisar.setBounds(x, y, 50, 50);
		} while (superposicionDeBoton(botonARevisar));
		nodosConCoordenadas.put(Integer.parseInt(texto), new Point(x, y));
		botonesDeNodos.add(botonARevisar);

	}

	private boolean superposicionDeBoton(JButton botonARevisar) {
		Rectangle areaBoton = botonARevisar.getBounds();
		for (JButton boton : botonesDeNodos) {
			Rectangle escudoDeBoton = new Rectangle(boton.getBounds());
			escudoDeBoton.setBounds(boton.getX(), boton.getY(), boton.getWidth() / 2 + 30, boton.getHeight() / 2 + 30);
			if (areaBoton.intersects(escudoDeBoton))
				return true;
		}
		return false;
	}

	public void dibujarLineas() {
		panel_grafo.repaint();
	}

	public void setVisible(boolean visible) {
		frame.setVisible(visible);
	}

	public void mostrarConjunto(int tipo) {

		Set<Integer> nodosDelConjunto = null;
		Color[] colores = { Color.green, Color.cyan, Color.pink, Color.yellow };
		switch (tipo) {
		case 0: {
			nodosDelConjunto = solucionesGolosas.get(0);
			break;
		}
		case 1: {
			nodosDelConjunto = solucionesGolosas.get(1);
			break;
		}
		case 2: {
			nodosDelConjunto = solucionesGolosas.get(2);
			break;
		}
		case 3: {
			nodosDelConjunto = solucionFuerzaBruta;
			break;
		}
		}
		
		for (int j = 0; j < botonesDeNodos.size(); j++) {
			Integer boton = Integer.parseInt(botonesDeNodos.get(j).getText());
			if(nodosDelConjunto.contains(boton))
				botonesDeNodos.get(j).setBackground(colores[tipo]);
		}
		
				
		

	}

	private void limpiarNodos() {
		for (JButton boton : botonesDeNodos) {
			boton.setBackground(null);
		}
	}

	@Override
	public void notificar() {
		this.nodos = managerSoluciones.obtenerNodos();
		this.aristas = managerSoluciones.obtenerArista();
		this.solucionesGolosas = managerSoluciones.getSoluciones();
		this.solucionFuerzaBruta = managerSoluciones.getSolucionConFuerzaBruta();

	}
}
