package vista;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;

import controlador.ArchivoJSON;
import controlador.Controlador;
import modelo.ManagerSoluciones;

import javax.sound.sampled.Clip;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Cursor;

public class Menu {

	private JFrame frame;
	private int nroVertice = 0;
	private List<Clip> enReproduccion;

	private Controlador controlador;
	private ManagerSoluciones managerDeSoluciones;

	public static void main(String[] args) {
		ManagerSoluciones managerDeSoluciones = new ManagerSoluciones();
		Controlador controlador = new Controlador(managerDeSoluciones);

		ArchivoJSON.cargarEnSistemaJSON("archivo.json");

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu window = new Menu(managerDeSoluciones, controlador);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Menu(ManagerSoluciones managerSoluciones, Controlador controlador) {
		this.managerDeSoluciones = managerSoluciones;
		this.controlador = controlador;
		initialize(managerSoluciones, controlador);
	}

	private void initialize(ManagerSoluciones managerSoluciones, Controlador controlador) {
		
		// Inicializaciones
		nroVertice = controlador.cantidadDeVertices();
		List<JButton> botonesMenu = new ArrayList<>();
		enReproduccion = new ArrayList<>();

		// Ventana
		frame = new JFrame();
		frame.getContentPane().setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		frame.setBounds(100, 100, 882, 706);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);


		// Panel
		JPanel panel = Efectos.crearFondo(frame, "/imagenes/fondoMenu.jpg");

		frame.getContentPane().add(panel);
		panel.setLayout(null);

		// Botones
		JButton btn_verGrafoyGrupos = new JButton("VER GRAFO");
		btn_verGrafoyGrupos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Efectos.reprodBgm("/sonidos/sf_click.wav", 1, enReproduccion);
				VerGrafo grafo = new VerGrafo(Menu.this, managerDeSoluciones, controlador);
				ManagerDeVentanas managerAGrafo = new ManagerDeVentanas(Menu.this, grafo);
				managerAGrafo.irAGrafo();
			}
		});
		btn_verGrafoyGrupos.setBounds(280, 503, 276, 50);
		botonesMenu.add(btn_verGrafoyGrupos);
		panel.add(btn_verGrafoyGrupos);

		JButton btn_conectarAristas = new JButton("CONECTAR VERTICES");
		btn_conectarAristas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Efectos.reprodBgm("/sonidos/sf_click.wav", 1, enReproduccion);
				ConectarVertices aristas = new ConectarVertices(Menu.this, controlador, managerDeSoluciones);
				ManagerDeVentanas managerAAristas = new ManagerDeVentanas(Menu.this, aristas);
				managerAAristas.irAAristas();
			}
		});
		btn_conectarAristas.setBounds(280, 358, 276, 50);
		botonesMenu.add(btn_conectarAristas);
		panel.add(btn_conectarAristas);

		JButton btn_agregarVertice = new JButton("AGREGAR VERTICE");
		btn_agregarVertice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Efectos.reprodBgm("/sonidos/sf_creada.wav", 1, enReproduccion);
				JOptionPane.showMessageDialog(frame, "Se ha creado el vertice: " + nroVertice, "Se creo un vertice",
						JOptionPane.INFORMATION_MESSAGE);
				controlador.agregarVertice(nroVertice);
				nroVertice++;
			}
		});
		btn_agregarVertice.setBounds(280, 216, 276, 50);
		botonesMenu.add(btn_agregarVertice);
		panel.add(btn_agregarVertice);

		// Estilo de botones
		Efectos.estiloDeBotones(botonesMenu);
		// Musica
		Efectos.reprodBgm("/sonidos/bgm_Grafo.wav", 2, enReproduccion);

	}

	public void setVisible(boolean visible) {
		frame.setVisible(visible);
	}

	public Controlador getControlador() {
		return controlador;
	}

}
