package controlador;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import modelo.Grafo;
import modelo.ManagerSoluciones;

public class ArchivoJSON {

	public Grafo grafo;

	public ArchivoJSON() {
		this.grafo = new Grafo();
	}

	public static void cargarEnSistemaJSON(String rutaArchivo) {
		try {
			Gson gson = new GsonBuilder().create();
			FileReader fileReader = new FileReader(rutaArchivo);
			Grafo grafo = gson.fromJson(fileReader, Grafo.class);
			ManagerSoluciones.grafo = grafo;
			fileReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void EscribirJsonJSON(String archivo, Grafo grafo) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(grafo);
		try {
			FileWriter writer = new FileWriter(archivo);
			writer.write(json);
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}