package controlador;

import modelo.ManagerSoluciones;

public class Controlador {
	private ManagerSoluciones managerSoluciones;

	public Controlador(ManagerSoluciones managerSoluciones) {
		this.managerSoluciones = managerSoluciones;
	}

	public void agregarVertice(int vertice) {
		managerSoluciones.agregarVertice(vertice);
		agregarEnJSON();
	}

	public void agregarArista(int v1, int v2) {
		managerSoluciones.agregarArista(v1, v2);
		agregarEnJSON();
	}
	public boolean validadArista(int origen,int destino) {
	    return ManagerSoluciones.grafo.validarArista(origen, destino);
	}
	private void agregarEnJSON() {
		ArchivoJSON.EscribirJsonJSON("archivo.json", managerSoluciones.grafo);
	}

	public void crearSoluciones() {
		managerSoluciones.crearSolucionesGolosos();
		managerSoluciones.crearSolucionFuerzaBruta();
	}

	public int cantidadDeVertices() {
		return managerSoluciones.obtenerNodos().size();
	}
}
